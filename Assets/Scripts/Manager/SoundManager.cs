﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource bgMusicSource;
    [SerializeField] private AudioSource gameOverMusicSource;
    [SerializeField] private AudioSource playerGunSource;
    [SerializeField] private AudioSource enemyGunSource;
    [SerializeField] private AudioSource explosionSource;

    public static SoundManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void BGPlay(AudioClip clip)
    {
        bgMusicSource.clip = clip;
        bgMusicSource.Play();
    }
    public void BGStop(AudioClip clip)
    {
        bgMusicSource.clip = clip;
        bgMusicSource.Stop();
    }

    public void GOPlay (AudioClip clip)
    {
        gameOverMusicSource.clip = clip;
        gameOverMusicSource.Play();
    }

    public void PlayerPlay(AudioClip clip)
    {
        playerGunSource.clip = clip;
        playerGunSource.Play();
    }
    public void EnemyPlay(AudioClip clip)
    {
        enemyGunSource.clip = clip;
        enemyGunSource.Play();
    }
    public void ExplosionPlay(AudioClip clip)
    {
        explosionSource.clip = clip;
        explosionSource.Play();
    }
}
