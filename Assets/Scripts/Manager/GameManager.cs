﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject playButton;
    [SerializeField] private GameObject playerShip;
    [SerializeField] private GameObject enemySpawn;
    [SerializeField] private GameObject gameOver;    
    [SerializeField] private GameObject restartButton;    
    [SerializeField] private GameObject quitButton;
    [SerializeField] private GameObject scoreText;
    [SerializeField] private GameObject victoryText;
    [SerializeField] private GameObject continueButton;

    public enum GameManagerState
    {
        Opening,
        Gameplay,
        Gameplay2,
        GameOver,
        Ending,
        Restart,
        Quit,
        Continue,
    }

    GameManagerState GMState;

    void Start()
    {
        GMState = GameManagerState.Opening;
    }

    void UpdateGameManagerState()
    {
        switch(GMState)
        {
            case GameManagerState.Opening:
                playButton.SetActive(true);
                restartButton.SetActive(false);
                quitButton.SetActive(false);
                gameOver.SetActive(false);
                break;

            case GameManagerState.Gameplay:
                scoreText.GetComponent<ScoreManager>().Score = 0;
                playButton.SetActive(false);
                playerShip.GetComponent<PlayerControl>().Init();
                enemySpawn.GetComponent<Spawner>().letSpawn();
                break;

            case GameManagerState.Gameplay2:
                SceneManager.LoadScene(1);
                scoreText.GetComponent<ScoreManager>().Score = 10;
                playButton.SetActive(false);
                playerShip.GetComponent<PlayerControl>().Init();
                enemySpawn.GetComponent<Spawner>().letSpawn();
                break;

            case GameManagerState.GameOver:
                enemySpawn.GetComponent<Spawner>().unSpawn();
                Invoke("EndingState", 1f);
                gameOver.SetActive(true);
                break;

            case GameManagerState.Ending:
                restartButton.SetActive(true);
                quitButton.SetActive(true);
                break;

            case GameManagerState.Restart:
                gameOver.SetActive(false);
                restartButton.SetActive(false);
                quitButton.SetActive(false);
                Invoke("StartGamePlay", 1f);
                break;

            case GameManagerState.Quit:
                Application.Quit();
                Debug.Log("Quit");
                break;

            case GameManagerState.Continue:
                enemySpawn.GetComponent<Spawner>().unSpawn();
                continueButton.SetActive(true);
                victoryText.SetActive(true);
                break;
        }
    }

    public void SetGameManagerState(GameManagerState state)
    {
        GMState = state;
        UpdateGameManagerState();
    }

    public void StartGamePlay()
    {
        GMState = GameManagerState.Gameplay;
        UpdateGameManagerState();
    }

    public void ChangeToOpeningState()
    {
        SetGameManagerState(GameManagerState.Opening);
    }

    public void EndingState()
    {
        GMState = GMState = GameManagerState.Ending;
        UpdateGameManagerState();
    }
    public void ReStartGamePlay()
    {
        GMState = GameManagerState.Restart;
        UpdateGameManagerState();
    }
    public void QuitGamePlay()
    {
        GMState = GameManagerState.Quit;
        UpdateGameManagerState();
    }
    public void ContinueGamePlay()
    {
        GMState = GameManagerState.Continue;
        UpdateGameManagerState();
    }

    public void StartGamePlay2()
    {
        GMState = GameManagerState.Gameplay2;
        UpdateGameManagerState();
    }
}
