﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    public GameObject GameManager;
    Text scoreTextUI;
    int score;

    public int Score
    {
        get
        {
            return this.score;
        }
        set
        {
            this.score = value;
            UpdateScoreTextUI();
        }
    }
    void Start()
    {
        scoreTextUI = GetComponent<Text>();
    }
    
    void UpdateScoreTextUI()
    {
        string scoreCurrent = string.Format("{0:0}", score);
        scoreTextUI.text = scoreCurrent;
        if (score == 10)
        {
            GameManager.GetComponent<GameManager>().SetGameManagerState(global::GameManager.GameManagerState.Continue);
        }
    }
}
