﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaser : MonoBehaviour
{
    [SerializeField] private float laserSpeed;

    void Start()
    {
        laserSpeed = 3f;
    }

    void Update()
    {
        Vector2 position = transform.position;

        position = new Vector2 (position.x, position.y + laserSpeed * Time.deltaTime);

        transform.position = position;

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        if (transform.position.y >= max.y)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "EnemyShip")
        {
            Destroy(gameObject);
        }
    }

    public void ChangeLaserSpeed()
    {
        laserSpeed += 2f;
    }
}
