﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    private PlayerLaser laserSpeed;
    public GameObject GameManager;
    [SerializeField] private float playerSpeed;
    [SerializeField] private Text livesUIText;
    [SerializeField] private GameObject laser;
    [SerializeField] private GameObject laserStart;
    [SerializeField] private GameObject Explosion;
    [SerializeField] private AudioClip bgSound;
    [SerializeField] private AudioClip gameOver;
    [SerializeField] private AudioClip playerGunSound;

    const int MaxLives = 1;
    int lives;

    public void Start()
    {
        GetComponent<AudioSource>();
    }

    public void Init()
    {
        lives = MaxLives;
        livesUIText.text = lives.ToString();
        gameObject.SetActive(true);
    }

    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        Vector2 direction = new Vector2(x, y).normalized;

        Move(direction);
    }

    void Move(Vector2 direction)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        min.x = min.x + 0.5f;
        max.x = max.x - 0.5f;

        min.y = min.y + 0.5f;
        max.y = max.y - 0.5f;

        Vector2 position = transform.position;

        position += direction * playerSpeed * Time.deltaTime;

        position.x = Mathf.Clamp(position.x, min.x, max.x);
        position.y = Mathf.Clamp(position.y, min.y, max.y);

        transform.position = position;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SoundManager.Instance.PlayerPlay(playerGunSound);
            GameObject laserBeam = (GameObject)Instantiate(laser);
            laserBeam.transform.position = laserStart.transform.position;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if ((collider.tag == "EnemyShip") || (collider.tag == "EnemyBullet"))
        {
            ExplosionAnim();
            lives--;
            livesUIText.text = lives.ToString();
            if (lives <= 0)
            {
                SoundManager.Instance.BGStop(bgSound);
                GameManager.GetComponent<GameManager>().SetGameManagerState(global::GameManager.GameManagerState.GameOver);
                SoundManager.Instance.GOPlay(gameOver);
                gameObject.SetActive(false);
            }
        }
    }

    void ExplosionAnim()
    {
        GameObject explosion = (GameObject)Instantiate(Explosion);
        explosion.transform.position = transform.position;
    }
}
