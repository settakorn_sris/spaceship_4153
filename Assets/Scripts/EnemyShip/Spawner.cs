﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemySpawn;
    [SerializeField] private float spawnTime;
    [SerializeField] private float nextEnemy;

    private void EnemySpawn()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject enemy = (GameObject)Instantiate(enemySpawn);
        enemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        otherEnemy();
    }

    private void otherEnemy()
    {
        if (spawnTime > 1f)
        {
            nextEnemy = Random.Range(1f, spawnTime);
        }
        else
        {
            Invoke("EnemySpawn", spawnTime);
        }
    }

    public void spawnRate()
    {
        if (spawnTime > 1f)
            --spawnTime;
        if (spawnTime == 1f)
            CancelInvoke("spawnRate");
    }

    public void letSpawn()
    {
        Invoke("EnemySpawn", spawnTime);
        InvokeRepeating("spawnRate", 0f, 30f);
    }

    public void unSpawn()
    {
        CancelInvoke("EnemySpawn");
        CancelInvoke("spawnRate");
    }
}
