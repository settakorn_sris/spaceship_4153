﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour
{
    [SerializeField] private GameObject enemyGun;
    [SerializeField] private AudioClip enemyGunSound;

    void Start()
    {
        Invoke("fireEnemyLaser", 1f);
    }

    private void fireEnemyLaser()
    {
        GameObject playerShip = GameObject.Find("Player");

        if (playerShip != null)
        {
            SoundManager.Instance.EnemyPlay(enemyGunSound);
            GameObject laser = (GameObject)Instantiate(enemyGun);
            laser.transform.position = transform.position;
            Vector2 direction = playerShip.transform.position - laser.transform.position;
            laser.GetComponent<EnemyLaser>().SetDirection(direction);
        }
    }
}
