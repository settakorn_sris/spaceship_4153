﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaser : MonoBehaviour
{
    [SerializeField] private float laserSpeed;
    Vector2 _direction;
    bool checkReady;

    void Awake()
    {
        checkReady = false;
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction.normalized;
        checkReady = true;
    }

    void Update()
    {
        if(checkReady)
        {
            Vector2 position = transform.position;

            position += _direction * laserSpeed * Time.deltaTime;

            transform.position = position;

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

            if ((transform.position.x < min.x) || (transform.position.x > max.x)
               || (transform.position.y < min.y) || (transform.position.y > max.y))
            {
                Destroy(gameObject);
            }
        }
    }

    void OnTriggerEnterEnter2D(Collider2D _collider)
    {
        if(_collider.tag == "PlayerShip")
        {
            Destroy(gameObject);
        }
    }
}
