﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    [SerializeField] private GameObject scoreText;
    [SerializeField] private float enemySpeed;
    [SerializeField] private GameObject Explosion;

    void Start()
    {
        scoreText = GameObject.FindGameObjectWithTag("ScoreText");
    }

    void Update()
    {
        Vector2 position = transform.position;

        position = new Vector2(position.x, position.y - enemySpeed * Time.deltaTime);

        transform.position = position;

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        if (transform.position.y <= min.y)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if ((collider.tag == "PlayerShip") || (collider.tag == "PlayerBullet"))
        {
            ExplosionAnim();
            Destroy(gameObject);
            scoreText.GetComponent<ScoreManager>().Score += 1;
        }
    }

    void ExplosionAnim()
    {
        GameObject explosion = (GameObject)Instantiate(Explosion);
        explosion.transform.position = transform.position;
    }
}
